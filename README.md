pz/pzd
======

Pz and pzd (parallel zip and parallel zip decompress) are a pair of bash scripts
written on Slackware Linux to compress and decompress large files using all
available CPU cores and threads on a multi-core system.

Copyright (C) 2016-2024 by Iain Nicholson. iain.j.nicholson@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Introduction
------------

These scripts are hacks, from an idea one day sitting in a lab with a poor
network connection. I had very large files to copy across the slow network and
invested about 30 minutes hacking together the prototypes which have now been
rewritten several times, hopefully with many improvements.

They are written in the Bourne Again Shell (bash) and, in their current form,
will only detect multiple CPUs on Linux since they use /proc/cpuinfo to count
the number of CPU cores (virtual) on the machine. This has been abstracted out
into a helper script called countcpus which output 1 if it can't actually see
/proc/cpuinfo.

Pz works by counting the number of virtual CPU cores on the current system and
splitting (using split) the input file into several chunks, one for each core.
The chunks are then compressed in parallel by the shell and gathered up into
a tar file with the same name as the input file and with the extension ".tarx"
appended. The chunks are numbered sequentially and could be decompressed and
concatenated manually.

Pzd automates the process of extracting the compressed data from the ".tarx"
file and recombining the decompressed data to recreated the original file.


Installation
------------

Since this is written as a group of shell scripts, no compilation is required.

Copy pz, pzd, pzLib, countcpus and datestamp to a suitable directory on your
system (for example ~/scripts/pz) and add this location to your PATH, e.g.:

export PATH=${PATH}:~/scripts/pz

Usage
-----

To compress file fred.iso:

  ```pz fred.iso```

This will produce fred.iso.tarx

To decompress fred.iso.tarx:

  ```pzd fred.iso.tarx```

To see all the command line options:

  ```pz -h```

and

  ```pzd -h```


Implementation Details
----------------------

Pz is configured to do its work under /tmp. It creates a subdirectory there
whose name consists of the name of the file being compressed with a date and
timestamp and a UUID (from uuidgen) appended. The working directory is deleted
when the script terminates successfully. Otherwise it is left as-is for
inspection of the wreckage.

The temporary directory's parent is specified with the environment variable
PZ_TMP, which is initialised in the script if not already set. The user can set
a different value on the command line if desired. Pzd uses PZD_TMP for the
equivalent decompression and reconstruction of the input file.

If processing is interrupted with ^C, the working directory will not be deleted.

Before attempting any compression, pz used to calculates the SHA1 checksum of
the input file and stores it in its working directory in sha1sums.txt. This
slowed the process down considerably, so now it is not done by default, although
should you require it, the -p and --paranoid command line options enable the
checksum. Pzd automatically verifies the checksum after decompression and
reassembly of the input file if the sha1checksums.txt file is present in the
archive.

The name of the input file is stored as a single line in input_name.txt.

The compression takes place in parallel on all virtual CPU cores unless
otherwise specified on the command line.

The compressed files, input_name.txt and any sha1checksums.txt are archived in
the ".tarx" file.

Finally, the working directory under /tmp is deleted.

Decompression using pzd is obviously the reverse. It creates a working directory
named after the ".tarx" file under /tmp, again with a date and timestamp
and UUID appended where it extracts the archive.

Using the sha1sums.txt file, it reads the name of the output file to be
reconstructed. If there is not sha1sums.txt file, it reads the name from
input_name.txt.

Each compressed file is then decompressed in parallel and then read in
ASCIIbetical order (the files have a numerical suffix) and concatenated into
the output file.

If sha1sums.txt is present, sha1sum is used to check that the checksum of the 
decompressed and reconstructed output file matches the original (before
splitting and compression).

Finally, the working directory under /tmp is deleted.

Regression Tests
----------------

The ```test``` subdirectory contains regression test scripts to verify the
behaviour of both scripts using input data of various sizes and using various
numbers of processes (cores/threads).

There are two top-level test scripts, ```test_all``` and ```test_all_checksums```
which do the compression without and with the SHA1 checksum of the input file,
respectively.

Both of these scripts run test_dataCreate to create the input data.

To run the tests, the test subdirectory must be appended to your PATH, e.g.:
  ```export PATH=${PATH}:~/scripts/pz/test```

To clean up after testing, run ```./test_clean```

Performance
-----------

So far, I have run the tests on four systems, all of which have mechanical
("spinning rust") hard disks. I expect disk I/O to be a bottleneck. Therefore,
the compression algorithms should ideally be doing a lot of work (using a lot
of CPU time) and outputting as small files as possible. The more CPUs/cores
there are fighting over the disk, the poorer the performance advantage.

If the number of CPU cores becomes great enough, then memory bandwidth will
also become a bottleneck.

You may like to try on various machines to see what the differences are. I have
tested on:
1. AMD Ryzen 9 5900X 12 cores/24 threads, 32GB RAM
2. AMD FX(tm)-8150, 8 cores, 16GB RAM
3. AMD Phenom(tm) II X6 1090T 6 cores, 6GB RAM
4. AMD Phenom(tm) II X6 1045T 6 cores, 8GB RAM.

The tests seem to run ~10% slower with the checksums enabled.

Bugs and Limitations
--------------------

Pz runs the compression jobs at the highest priority. It should really run them
nice -n 19 to be considerate to other users logged into the system

It does not consider small files (for example, small enough not to provode at least one byte to each core).


TO-DO:
* Better documentation
* Tidy up cleanly when someone does ^C
* Make it behave more like gzip on the command line.

