#!/bin/bash
#
# Run all of the pz and pzd test scripts.
#
# Sat Sep  7 15:43:22 BST 2024
#
# Copyright 2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# Modification history:
# 2024-08-28 Initial creation. Extracted from test_all.
#

export PATH=${PATH}:..:.
PASSES=0
FAILURES=0

./test_clean
echo "Creating input data."
./test_dataCreate

echo "Running tests."
for t in test_pz_cs \
         test_pz_1k_to_1G_cs \
         test_pz_1k_to_1G_threads_1_to_many_cs
do
    echo -n "${t}: "
    #/usr/bin/time -v ./${t}
    ./${t}
    RESULT=$?
    if [ 0 == ${RESULT} ]
    then
        echo "PASS"
        (( PASSES++ ))
    else
        echo "FAIL"
        (( FAILURES++ ))
    fi
done

if [ ${FAILURES} -gt 0 ]
then
    echo "FAILURES: ${FAILURES}"
    exit 2
fi

