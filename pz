#!/bin/bash
#
# pz - Script to split a large file into chunks, compress the chunks in parallel
#      and wrap them up in a tarball.
#
# 
# Copyright 2016-2024 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# This comes with no warranty. It is a dirty hack.
#
# Change history:
# 2016-04-20 Initial creation.
# 2022-11-12 Renamed to pz.
#            Common functions extracted to pzLib.
#            Licensed under the Apache License, Version 2.0.
# 2022-11-13 Try lzip.
# 2022-11-16 Add command line options to select compression type, number of CPU
#            cores to use and number of chunks to split the input file into.
# 2024-08-25 Improve file name and directory handling.
# 2024-08-27 Make it a bit less stupid about its use of the working directory.
# 2024-08-28 Fix smelling mistoke.
# 2024-09-07 To make it go faster, don't calculate checksums by default. Add
#            --paranoid option to calculate checksums.
# 2024-09-08 Verbose option.
#

MYNAME=`basename $0`
PZ_TMP=${PZ_TMP:-/tmp}

#
# Include library functions.
#
. pzLib

#
# Print usage information.
#
usage()
{
    echo "${MYNAME}: Split a file into chunks and compress the chunks in parallel. Put the chunks in a tarball with the extension '.tarx'."
    echo
    echo "Usage:"
    echo "    ${MYNAME} [options] <file>"
    echo
    echo "Output:"
    echo "    <file>.tarx"
    echo
    echo "Options:"
    echo "    -n | --ncpus  <number>    Set the number of CPU cores to use."
    echo "    -c | --chunks <number>    Set the number of chunks to use."
    echo "    -g | --gzip               Use gzip for compression."
    echo "    -b | --bzip2              Use bzip2 for compression."
    echo "    -x | --xz                 Use xz for compression."
    echo "    -l | --lzip               Use lzip for compression."
    echo "    -p | --paranoid           Calculate SHA1 checksums for the input file."
    echo "    -v | --verbose            Print information about what's happening."
    echo "    -h | --help               Show this help information."
    echo
    echo "By default, ${MYNAME} uses all available cores/threads on the system and uses lzip for compression. The number of chunks is equal to the number of CPU cores."
    echo
}

#
# Process command line options.
#
process_opts()
{
    while [ $# -ge 1 ]
    do
        PARAM=$1
        #echo "${PARAM}"
        if [ ${PARAM:0:2} == "--" ]
        then
            case ${PARAM} in
                "--ncpus" )
                    export NCPUS=$2
                    shift
                    shift
               ;;
                "--chunks" )
                    export CHUNKS=$2
                    shift
                    shift
               ;;
                "--gzip" )
                    export COMPRESS=gzip
                    shift
               ;;
                "--bzip2" )
                    export COMPRESS=bzip2
                    shift
               ;;
                "--xz" )
                    export COMPRESS=xz
                    shift
               ;;
                "--lzip" )
                    export COMPRESS=lzip
                    shift
               ;;
                "--paranoid" )
                    export CHECKSUMS=1
                    shift
               ;;
                "--verbose" )
                    export VERBOSE=1
                    shift
               ;;
               "--help" )
                   usage
                   exit 2
               ;;
               * )
                   abort "Unrecognised option ${PARAM}"
                ;;
            esac
        elif [ ${PARAM:0:1} == "-" ]
        then
            case ${PARAM} in
                "-n" )
                    export NCPUS=$2
                    shift
                    shift
               ;;
                "-c" )
                    export CHUNKS=$2
                    shift
                    shift
               ;;
                "-g" )
                    export COMPRESS=gzip
                    shift
               ;;
                "-b" )
                    export COMPRESS=bzip2
                    shift
               ;;
                "-x" )
                    export COMPRESS=xz
                    shift
               ;;
                "-l" )
                    export COMPRESS=lzip
                    shift
               ;;
                "-p" )
                    export CHECKSUMS=1
                    shift
               ;;
                "-v" )
                    export VERBOSE=1
                    shift
               ;;
               "-h" )
                   usage
                   exit 2
               ;;
               * )
                   abort "Unrecognised option ${PARAM}"
                ;;
            esac
        else
            export INPUT="${PARAM}"
            shift
        fi
    done 
}

#
# pz needs to know where all its helper scripts are.
#
check_helpers()
{
    for helper in pzLib \
                  datestamp \
                  countcpus
    do
        which "${helper}" 2>&1 | grep '^which: no' > /dev/null
        if [ $? == 0 ]
        then
            echo "ERROR: ${helper} not found. Please update PATH."
            exit 2
        fi
    done
}

#
# Check that all the external utilities are present.
#
check_utils()
{
    for util in gzip\
                bzip2 \
                xz \
                lzip \
                split \
                readlink \
                dirname \
                sha1sum \
                uuidgen
    do
        which "${util}" 2>&1 | grep '^which: no' > /dev/null
        if [ $? == 0 ]
        then
            echo "ERROR: ${util} not found. Please install."
            exit 2
        fi
    done
}

#
# Start here.
# Set defaults
#
if [ $# -lt 1 ]
then
    usage
    exit 2
fi
WORKROOT="${PZ_TMP}"
COMPRESS=lzip
UUID=`uuidgen`
NCPUS=`countcpus`
CHECKSUMS=0
VERBOSE=0

check_helpers
check_utils

#
# This needs to be fixed so that we can process multiple input files.
#
process_opts $@

DATESTAMP=`datestamp`

if [ ! -f "${INPUT}" ]
then
    abort "${INPUT} does not exist or is not a file."
fi

TRUEINPUT=`readlink -f ${INPUT}`
NAME_ONLY=`basename ${INPUT}`
WORKDIR="${WORKROOT}/${NAME_ONLY}-${DATESTAMP}-${UUID}"

OUTPUTDIR=`pwd`

mkdir "${WORKDIR}"
if [ ! -d "${WORKDIR}" ]
then
    abort "Failed to create temporary directory ${WORKDIR}"
fi

if [ -z "${CHUNKS}" ]
then
    CHUNKS=${NCPUS}
fi

if [ ${CHUNKS} -lt ${NCPUS} ]
then
    NCPUS=${CHUNKS}
fi

if [ "${VERBOSE}" == "1" ]
then
    echo "Number of CPU cores: ${NCPUS}"
    echo "Number of chunks: ${CHUNKS}"
fi

#
# We should really check the size of the input file and refuse to split it up
# into chunks if it's not very big.
#
pushd "${WORKDIR}" > /dev/null
PARALLEL=""

#
# Is this over-engineering? Does a single CPU need to be a special case?
# Does it go any faster? Two processes are fighting over the same file!
# Does it go slower?
#
if [ "${NCPUS}" -gt 1 ]
then
    if [ "${CHECKSUMS}" == "1" ]
    then
        sha1sum "${TRUEINPUT}" > "sha1sums.txt" &
        PARALLEL="$!"
    fi
    echo `basename "${TRUEINPUT}"` > input_name.txt
    split -n ${CHUNKS} -d "${TRUEINPUT}" "${NAME_ONLY}." &
    PARALLEL="${PARALLEL} $!"
    wait ${PARALLEL}
else
    if [ "${CHECKSUMS}" == "1" ]
    then
        sha1sum "${TRUEINPUT}" > "sha1sums.txt"
    fi
    echo `basename "${TRUEINPUT}"` > input_name.txt
    split -n ${CHUNKS} -d "${TRUEINPUT}" "${NAME_ONLY}."
fi

#
# Remove the path from the filename in the checksums file.
#
INPUTPATH=`dirname ${TRUEINPUT}`
if [ "${CHECKSUMS}" == "1" ]
then
    sed -i "s,${INPUTPATH}/,," "sha1sums.txt"
fi

#
# Compress in parallel using up to NCPUS
#
ls ${NAME_ONLY}.* | (
COUNT=0
PARALLEL=""
while read FILE
do
    if [ ${COUNT} -lt ${NCPUS} ]
    then
        ${COMPRESS} ${FILE} &
        PARALLEL="${PARALLEL} $!"
        COUNT=$(( ${COUNT} + 1 ))
    else
        wait -n
        ${COMPRESS} ${FILE} &
        PARALLEL="${PARALLEL} $!"
    fi
done
wait ${PARALLEL}
)

FILES=`ls`

tar cf "${OUTPUTDIR}/${NAME_ONLY}".tarx ${FILES}
if [ $? == 2 ]
then
    abort "Tar exited with a fatal error."
fi

if [ ! -f "${OUTPUTDIR}/${NAME_ONLY}".tarx ]
then
    abort "Failed to create ${INPUT}.tarx"
fi
popd > /dev/null

rm -Rf "${WORKDIR}"

